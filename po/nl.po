# Dutch translation of dfarc.
# Copyright (C) 2014 Free Software Foundation, Inc.
# This file is distributed under the same license as the dfarc package.
# Koen Torfs <koen@drunkfelines.com>, 2010, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: dfarc-3.11-pre1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-09-01 20:58+0200\n"
"PO-Revision-Date: 2014-09-06 04:02+0100\n"
"Last-Translator: Koen Torfs <koen@drunkfelines.com>\n"
"Language-Team: Dutch <vertaling@vrijschrift.org>\n"
"Language: nl\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/BZip.cpp:303
#, c-format
msgid "Error: Could not open input file '%s' for bzip decompression."
msgstr "Fout: Kon invoerbestand '%s' niet openen voor bzip-decompressie."

#: ../src/BZip.cpp:318
msgid "Error: Invalid .dmod file selected!"
msgstr "Fout: Ongeldig .dmod-bestand geselecteerd!"

#: ../src/BZip.cpp:328
#, c-format
msgid "Error: Could not write to '%s'."
msgstr "Fout: Kon niet naar '%s' schrijven."

#: ../src/BZip.cpp:360
msgid "Invalid .dmod file specified."
msgstr "Ongeldig .dmod-bestand gespecificeerd."

#: ../src/BZip.cpp:364
msgid "Critical program function error: opened for write."
msgstr "Kritieke programmafunctiefout: geopend voor schrijven."

#: ../src/BZip.cpp:368
msgid "Could not read .dmod file."
msgstr "Kon .dmod-bestand niet lezen."

#: ../src/BZip.cpp:372
msgid "Incomplete .dmod file. Please download it again."
msgstr "Onvolledig .dmod-bestand. U zal het opnieuw moeten downloaden."

#: ../src/BZip.cpp:376
msgid "The .dmod file is corrupted.  Please download it again."
msgstr "Het .dmod-bestand is beschadigd. U zal het opnieuw moeten downloaden."

#: ../src/BZip.cpp:380
msgid "The file is not a valid .dmod file."
msgstr "Het bestand is geen geldig .dmod-bestand."

#: ../src/BZip.cpp:384
msgid "Out of memory error."
msgstr "Onvoldoende geheugen beschikbaar."

#: ../src/BZip.cpp:388
msgid "An unhandled error occured."
msgstr "Er is een onafgehandelde fout opgetreden."

#: ../src/Config.cpp:202
msgid "Error: dinksmallwood.ini not found. Please run the main game and try running this program again."
msgstr "Fout: dinksmallwood.ini is niet gevonden. Start het hoofdspel en probeer dan dit programma opnieuw te starten."

#: ../src/Config.cpp:210
msgid "Error opening dinksmallwood.ini"
msgstr "Fout bij het openen van dinksmallwood.ini"

#: ../src/DFArcFrame.cpp:174
#, c-format
msgid ""
"DFArc version %s\n"
"Copyright (C) 2004  Andrew Reading (merlin)\n"
"Copyright (C) 2005, 2006  Dan Walma (redink1)\n"
"Copyright (C) 2008-2014  Sylvain Beucler (Beuc)\n"
"Build Date: %s\n"
"Powered by bzip2 (bzip.org) and wxWidgets (wxwidgets.org)"
msgstr ""
"DFArc versie %s\n"
"Copyright (C) 2004  Andrew Reading (merlin)\n"
"Copyright (C) 2005, 2006  Dan Walma (redink1)\n"
"Copyright (C) 2008-2014  Sylvain Beucler (Beuc)\n"
"Bouwdatum: %s\n"
"Gebruikt bzip2 (bzip.org) en wxWidgets (wxwidgets.org)"

#: ../src/DFArcFrame.cpp:181
msgid "About DFArc v3"
msgstr "Over DFArc v3"

#. Default logo (currently all black with a question mark)
#. TRANSLATORS: please make this SHORT, possibly rephrasing as "<
#. Choose!". This is included in the 160x120px logo box in the main
#. window and it doesn't word-wrap.
#: ../src/DFArcFrame.cpp:253
msgid "< Pick a D-Mod"
msgstr "< Kies een D-Mod"

#: ../src/DFArcFrame.cpp:318
msgid "No translations"
msgstr "Geen vertalingen beschikbaar"

#. Not displaying the default language explicitely, because it
#. makes the user think that a translation is always available.
#. mGameLocaleList->Insert(wxString(_("Default language")) + wxT(" (") + cur_locale_name + wxT(")"), 0);
#: ../src/DFArcFrame.cpp:325 ../src/Options.cpp:118
msgid "Don't translate"
msgstr "Geen vertaling gebruiken"

#: ../src/DFArcFrame.cpp:348
msgid "D-Mod files (*.dmod)"
msgstr "D-Mod bestanden (*.dmod)"

#: ../src/DFArcFrame.cpp:349
msgid "Select a .dmod file"
msgstr "Een .dmod-bestand selecteren"

#: ../src/DFArcFrame.cpp:544
#, c-format
msgid "The '%s' locale is not installed on your computer (locales tells the computer how to manage a language). You need to install it - check your system documentation."
msgstr "De taalregio ‘%s’ is niet op uw computer geïnstalleerd (taalregio's vertellen de computer hoe zich aan een taal aan te passen). Bekijk uw systeemdocumentatie om te leren hoe u de juiste taalregio kunt installeren."

#: ../src/DFArcFrame.cpp:547 ../src/DFArcFrame.cpp:625
msgid "Warning"
msgstr "Waarschuwing"

#: ../src/DFArcFrame.cpp:587
#, c-format
msgid "Dink Smallwood failed! Error code %d."
msgstr "Fout bij uitvoeren van Dink Smallwood! Foutcode %d."

#: ../src/DFArcFrame.cpp:589 ../src/DFArcFrame.cpp:594
#: ../src/DFArcFrame.cpp:655 ../src/DFArcFrame.cpp:657
#: ../src/DFArcFrame.cpp:700 ../src/DFArcFrame.cpp:709
msgid "Error"
msgstr "Fout"

#: ../src/DFArcFrame.cpp:591
#, c-format
msgid "Dink Smallwood ('%s') was not found on your computer. Please configure the Dink program name in the Options menu."
msgstr "Dink Smallwood ('%s') werd niet op uw computer gevonden. Stel de Dink-programmanaam in in het menu Opties."

#: ../src/DFArcFrame.cpp:622
msgid "Dinkedit saves all changes automatically. Altering maps can ruin the game. Are you sure you want to continue?"
msgstr "Dinkedit bewaart automatisch alle wijzigingen. Mappen aanpassen kan het spel onbruikbaar maken. Weet u zeker dat u verder wilt gaan?"

#: ../src/DFArcFrame.cpp:652
#, c-format
msgid "The editor ('%s') was not found on your computer. Please configure the editor program name in the Options menu."
msgstr "De editor ('%s') werd niet gevonden op uw computer. Stel de editorprogrammanaam in in het menu Opties."

#: ../src/DFArcFrame.cpp:657
msgid "Error while running the editor"
msgstr "Fout bij het uitvoeren van de editor"

#. FreeDesktop
#. Gnome
#. KDE
#. Xfce
#: ../src/DFArcFrame.cpp:698
msgid "Could not find a file manager (tried 'xdg-open', 'nautilus', 'konqueror' and 'thunar')"
msgstr "Kan geen bestandsbeheerder vinden (heb 'xdg-open', 'nautilus', 'konqueror' en 'thunar' geprobeerd)"

#: ../src/DFArcFrame.cpp:706
#, c-format
msgid "Cannot start '%s', please check your configuration in the Options window."
msgstr "Kan '%s' niet starten; controleer uw instellingen in het venster Opties."

#: ../src/DFArcFrame.cpp:716
msgid ""
"Welcome to DFArc, the Dink Smallwood front end!\n"
"\n"
"You can choose to play the original game (Dink Smallwood) or Dink-Modules (D-Mods) which contain new adventures.\n"
"\n"
"After completing the main game, give some D-Mods a try.\n"
"There are hundreds of them, just click File-Download D-Mods."
msgstr ""
"Welkom bij DFArc, de Dink Smallwood frontend!\n"
"\n"
"U kan kiezen om het originele spel (Dink Smallwood) te spelen, of Dink-Modules (D-Mods) die nieuwe avonturen bevatten.\n"
"\n"
"U zou een paar D-Mods moeten uitproberen na het beëindigen van het hoofdspel.\n"
"U vindt er honderden via Bestand – D-Mods downloaden."

#: ../src/DFArcFrame.cpp:723
msgid "Introduction"
msgstr "Introductie"

#. If there's no directory, let's not override
#: ../src/DFArcFrame.cpp:866
msgid "Cannot use the overridden Dink Smallwood directory - ignoring it. (permission problem?)"
msgstr "Kan de vervangen Dink Smallwood-map niet gebruiken – wordt genegeerd. (Toegangsrechtenprobleem?)"

#: ../src/DFArcFrame.cpp:868 ../src/DFArcFrame.cpp:876
msgid "Configuration error"
msgstr "Configuratiefout"

#. If there's no directory, let's not override
#: ../src/DFArcFrame.cpp:875
msgid "The Dink Smallwood directory you entered does not exist - ignoring it."
msgstr "De Dink Smallwood-map die u hebt opgegeven bestaat niet – wordt genegeerd."

#: ../src/DFArcFrame.cpp:890
msgid "You must select the uninstall option from the start menu to uninstall the main game."
msgstr "U moet de de-installatie-optie vanuit het startmenu kiezen om het hoofdspel te verwijderen."

#: ../src/DFArcFrame.cpp:891
msgid "Uninstall - Error"
msgstr "De-installatie – Fout"

#: ../src/DFArcFrame.cpp:896
msgid "Do you want to remove all save game files?"
msgstr "Wilt u alle bewaarde spellen verwijderen?"

#: ../src/DFArcFrame.cpp:897
msgid "Uninstall - Save Game Files"
msgstr "De-installeren – Spellen bewaren"

#: ../src/DFArcFrame.cpp:919
msgid "Unable to remove D-Mod directory. All other files were removed."
msgstr "Kan D-Modmap niet verwijderen. Alle andere bestanden werden met succes verwijderd."

#: ../src/DFArcFrame.cpp:925
msgid "D-Mod successfully uninstalled"
msgstr "D-Mod is gede-installeerd"

#: ../src/DFArcFrame.cpp:926
msgid "Uninstall - Success"
msgstr "De-installatie – Gelukt"

#: ../src/InstallVerifyFrame.cpp:56
msgid "Preparing"
msgstr "Bezig met voorbereiden"

#: ../src/InstallVerifyFrame.cpp:57
msgid "The D-Mod archive is being decompressed in a temporary file."
msgstr "Het D-Mod-archief wordt gedecomprimeerd naar een tijdelijk bestand."

#: ../src/InstallVerifyFrame.cpp:81
#, c-format
msgid ""
"No Description Available.\n"
"\n"
"The D-Mod will be installed in subdirectory '%s'."
msgstr ""
"Geen Beschrijving Beschikbaar.\n"
"\n"
"De D-Mod zal worden geïnstalleerd in submap '%s'."

#: ../src/InstallVerifyFrame.cpp:94
msgid "DFArc - Install D-Mod - "
msgstr "DFArc – D-Mod installeren – "

#: ../src/InstallVerifyFrame.cpp:143
msgid "DFArc - Installing"
msgstr "DFArc – Bezig met installeren…"

#: ../src/InstallVerifyFrame.cpp:157
msgid "The D-Mod you selected"
msgstr "De D-Mod die u geselecteerd hebt"

#: ../src/InstallVerifyFrame.cpp:159
msgid " was successfully installed."
msgstr "werd met succes geinstalleerd."

#: ../src/InstallVerifyFrame.cpp:160 ../src/Package.cpp:167
msgid "Success"
msgstr "Gelukt"

#: ../src/InstallVerifyFrame.cpp:169
msgid "An error occured while extracting the .dmod file."
msgstr "Er is een fout opgetreden bij het uitpakken van het .dmod-bestand."

#: ../src/Options.cpp:113
msgid "System language"
msgstr "Systeemtaal"

#: ../src/Options.cpp:136
msgid "Custom"
msgstr "Aangepast"

#: ../src/Options.cpp:175
msgid "Choose the Dink Smallwood install directory"
msgstr "Kies de Dink Smallwood-installatiemap."

#: ../src/Options.cpp:183
msgid "Choose a folder containing D-Mods"
msgstr "Kies een map met D-Mods"

#: ../src/Package.cpp:123
msgid "You must provide an identifier filename."
msgstr "U moet een identificerende bestandsnaam opgeven."

#: ../src/Package.cpp:139
msgid "Packaging"
msgstr "Bezig met verpakken…"

#: ../src/Package.cpp:139
msgid "The D-Mod is being packaged."
msgstr "De D-Mod wordt verpakt."

#. Success dialog
#: ../src/Package.cpp:165
#, c-format
msgid "%s was successfully packaged (compression ratio %2.1f : 1)."
msgstr "%s werd met succes verpakt (compressieverhouding %2.1f : 1)."

#: ../src/Package.cpp:172
msgid "Packaging aborted - removing partial .dmod file."
msgstr "Het verpakken is afgebroken – gedeeltelijk .dmod-bestand wordt verwijderd."

#: ../src/Package.cpp:173
msgid "Abort"
msgstr "Afbreken"

#: ../src/RecursiveDelete.cpp:55 ../src/RecursiveDelete.cpp:73
#, c-format
msgid "Could not remove %s"
msgstr "Kon %s niet verwijderen"

#: ../src/Tar.cpp:95
msgid "Listing files..."
msgstr "Bestandslijst wordt opgemaakt…"

#: ../src/Tar.cpp:134
msgid "Initializing..."
msgstr "Bezig met initialiseren…"

#: ../src/Tar.cpp:138
#, c-format
msgid "Error: Could not open tar file '%s' for bzip compression."
msgstr "Fout: Kon tar-bestand '%s' niet openen voor bzip-compressie."

#: ../src/Tar.cpp:160
msgid "Error: Could not initialize compression method!  Will not generate a correct .dmod file.  Quitting."
msgstr "Fout: Kon compressiemethode niet initialiseren! Zal geen correct .dmod-bestand genereren. Wordt afgesloten."

#: ../src/Tar.cpp:193
#, c-format
msgid "Error: File '%s' not found!  Cannot archive file."
msgstr "Fout: Bestand '%s' is niet gevonden! Kan bestand niet archiveren."

#. Close the output file.
#: ../src/Tar.cpp:243
msgid "Closing..."
msgstr "Bezig met afsluiten…"

#: ../src/Tar.cpp:363
#, c-format
msgid "Error: File '%s' not found!  Cannot read data."
msgstr "Fout: bestand '%s' is niet gevonden!  Kan geen gegevens lezen."

#. Nope.  Exit.
#: ../src/Tar.cpp:400
msgid "Error: This .dmod file has an invalid checksum!  Cannot read file."
msgstr "Fout: Dit .dmod-bestand heeft een ongeldige controlesom!  Kan bestand niet lezen."

#: ../src/Tar.cpp:484
#, c-format
msgid "Error: File '%s' not found!  Cannot extract data."
msgstr "Fout: Bestand '%s' is niet gevonden! Kan geen gegevens uitpakken."

#: ../src/Tar.cpp:494
#, c-format
msgid "Error: Cannot create directory '%s'.  Cannot extract data."
msgstr "Fout: kan map '%s' niet aanmaken. Kan gegevens niet uitpakken."

#: ../src/Tar.cpp:552
#, c-format
msgid "Got bad file %d/%d.  Skipping."
msgstr "Beschadigd bestand %d/%d. Wordt overgeslagen."

#: ../src/Tar.cpp:574
#, c-format
msgid "Error: Improperly archived file '%s'.  Skipping."
msgstr "Fout: Incorrect gearchiveerd bestand '%s'. Wordt overgeslagen."

#: ../src/Tar.cpp:607
msgid "Done."
msgstr "Klaar."

#: ../src/DFArcFrame_Base.cpp:27
msgid "&Open D-Mod to Install"
msgstr "&Te installeren D-Mod openen"

#: ../src/DFArcFrame_Base.cpp:27
msgid "Select a D-Mod to install"
msgstr "Selecteer een te installeren D-Mod"

#: ../src/DFArcFrame_Base.cpp:28
msgid "&Download D-Mods"
msgstr "&D-Mods downloaden"

#: ../src/DFArcFrame_Base.cpp:28
msgid "Go to The Dink Network to download some D-Mods!"
msgstr "Naar het Dink-netwerk gaan om enkele D-Mods te downloaden!"

#: ../src/DFArcFrame_Base.cpp:30
msgid "E&xit"
msgstr "E&xit"

#: ../src/DFArcFrame_Base.cpp:30
msgid "Exit DFArc"
msgstr "DFArc afsluiten"

#: ../src/DFArcFrame_Base.cpp:31
msgid "&File"
msgstr "&Bestand"

#: ../src/DFArcFrame_Base.cpp:33
msgid "&Refresh D-Mod List"
msgstr "D-Modlijst &verversen"

#: ../src/DFArcFrame_Base.cpp:33
msgid "Refreshes the D-Mod list for any new additions"
msgstr "Ververst de D-Modlijst voor nieuwe toevoegingen"

#: ../src/DFArcFrame_Base.cpp:34
msgid "&Browse Selected D-Mod Directory"
msgstr "Geselecteerde D-Modmap door&bladeren."

#: ../src/DFArcFrame_Base.cpp:34
msgid "Browse to the directory containing the current D-Mod"
msgstr "Naar de map die de huidige D-Mod bevat navigeren"

#: ../src/DFArcFrame_Base.cpp:35
msgid "&Uninstall Selected D-Mod"
msgstr "Geselecteerde D-Mod &de-installeren"

#: ../src/DFArcFrame_Base.cpp:35
msgid "Uninstalls the selected D-Mod"
msgstr "De-installeert de geselecteerde D-Mod"

#: ../src/DFArcFrame_Base.cpp:37
msgid "&Options"
msgstr "&Opties"

#: ../src/DFArcFrame_Base.cpp:37
msgid "View or modify DFArc options"
msgstr "DFArc-opties bekijken of wijzigen"

#: ../src/DFArcFrame_Base.cpp:38
msgid "&Edit"
msgstr "Be&werken"

#: ../src/DFArcFrame_Base.cpp:40
msgid "&Introduction"
msgstr "&Introductie"

#: ../src/DFArcFrame_Base.cpp:40
msgid "A quick introduction to Dinking and D-Mods"
msgstr "Een snelle introductie tot Dinken en D-Mods"

#: ../src/DFArcFrame_Base.cpp:41
msgid "&Walkthroughs and Guides"
msgstr "W&alkthroughs en gidsen"

#: ../src/DFArcFrame_Base.cpp:41
msgid "Stuck in a D-Mod? Check out The Dink Smallwood Solutions."
msgstr "Zit u vast in een D-Mod? Bekijk de Dink Smallwood Oplossingen."

#: ../src/DFArcFrame_Base.cpp:42
msgid "&Forums"
msgstr "&Fora"

#: ../src/DFArcFrame_Base.cpp:42
msgid "Ask a question on the forums, or see if someone had the same problem."
msgstr "Stel een vraag in de fora, of vind iemand met hetzelfde probleem."

#: ../src/DFArcFrame_Base.cpp:44
msgid "&About"
msgstr "&Info"

#: ../src/DFArcFrame_Base.cpp:44
msgid "About DFArc"
msgstr "Over DFArc"

#: ../src/DFArcFrame_Base.cpp:45
msgid "&Help"
msgstr "&Hulp"

#: ../src/DFArcFrame_Base.cpp:50
msgid "Play"
msgstr "Spelen"

#: ../src/DFArcFrame_Base.cpp:51
msgid "True Color"
msgstr "Ware kleuren"

#: ../src/DFArcFrame_Base.cpp:52
msgid "Windowed"
msgstr "Venstermodus"

#: ../src/DFArcFrame_Base.cpp:53
msgid "Sound"
msgstr "Geluid"

#: ../src/DFArcFrame_Base.cpp:54
msgid "Joystick"
msgstr "Joystick"

#: ../src/DFArcFrame_Base.cpp:55
msgid "Debug"
msgstr "Debuggen"

#: ../src/DFArcFrame_Base.cpp:56
msgid "v1.07 mode"
msgstr "v1.07-modus"

#: ../src/DFArcFrame_Base.cpp:57
msgid "Edit"
msgstr "Bewerken"

#: ../src/DFArcFrame_Base.cpp:58 ../src/Package_Base.cpp:27
msgid "Package"
msgstr "Pakket"

#. begin wxGlade: DFArcFrame_Base::set_properties
#: ../src/DFArcFrame_Base.cpp:73
msgid "DFArc v3"
msgstr "DFArc v3"

#: ../src/DFArcFrame_Base.cpp:83
msgid "v1.07 compatibility mode, for D-Mods released before 2006"
msgstr "v1.07-compatibiliteitsmodus, voor D-Mods uitgebracht voor 2006"

#. begin wxGlade: InstallVerifyFrame_Base::InstallVerifyFrame_Base
#: ../src/InstallVerifyFrame_Base.cpp:23
msgid "Preparing..."
msgstr "Bezig met voorbereiden…"

#: ../src/InstallVerifyFrame_Base.cpp:25
msgid "Main Dink directory"
msgstr "Dink-hoofdmap"

#: ../src/InstallVerifyFrame_Base.cpp:26 ../src/Options_Base.cpp:29
msgid "Additional D-Mods directory"
msgstr "Aanvullende D-Modmap"

#: ../src/InstallVerifyFrame_Base.cpp:28
msgid "Where do you want to install this D-Mod?"
msgstr "Waar wilt u deze D-Mod installeren?"

#: ../src/InstallVerifyFrame_Base.cpp:29
msgid "Install"
msgstr "Installeren"

#. begin wxGlade: InstallVerifyFrame_Base::set_properties
#: ../src/InstallVerifyFrame_Base.cpp:41
msgid "DFArc - Install D-Mod"
msgstr "DFArc – D-Mod installeren"

#. begin wxGlade: Options_Base::Options_Base
#: ../src/Options_Base.cpp:23
msgid "Close DFArc on play"
msgstr "DFArc afsluiten wanneer het spel begint"

#: ../src/Options_Base.cpp:24
msgid "Show developer buttons"
msgstr "Ontwikkelaarsknoppen tonen"

#: ../src/Options_Base.cpp:25
msgid "The engine updates dinksmallwood.ini on run (deprecated)"
msgstr "De engine actualiseert dinksmallwood.ini bij het uitvoeren (verouderd)"

#: ../src/Options_Base.cpp:26
msgid "Override the Dink Smallwood directory"
msgstr "De Dink Smallwood-map vervangen"

#: ../src/Options_Base.cpp:28 ../src/Options_Base.cpp:31
msgid "Browse"
msgstr "Navigeren"

#: ../src/Options_Base.cpp:32
msgid "Game program name"
msgstr "Naam van spelprogramma"

#: ../src/Options_Base.cpp:35
msgid "Editor program name"
msgstr "Naam van editorprogramma"

#: ../src/Options_Base.cpp:38
msgid "Preferred file browser"
msgstr "Voorkeursbestandsbeheerder"

#: ../src/Options_Base.cpp:40
msgid ""
"DFArc language\n"
"(restart DFArc to apply)"
msgstr ""
"DFArc-taal\n"
"(herstart DFArc om deze keuze toe te passen)"

#. begin wxGlade: Options_Base::set_properties
#: ../src/Options_Base.cpp:56
msgid "DFArc - Options"
msgstr "DFArc – Opties"

#. begin wxGlade: Package_Base::Package_Base
#: ../src/Package_Base.cpp:23
msgid "Identifier"
msgstr "Identificatie"

#: ../src/Package_Base.cpp:24
#, c-format
msgid ""
"Note: Unless you're making a D-Mod, you shouldn't be around here.\n"
"This is for creating a .dmod file for a D-Mod you made.\n"
"\n"
"Will package '%s' located at\n"
"%s"
msgstr ""
"Nota: Tenzij u een D-Mod aan het maken bent, zou u hier niet moeten zijn.\n"
"Dit is voor het aanmaken van een .dmod-bestand voor een D-Mod die u gecreëerd hebt.\n"
"\n"
"Zal '%s' verpakken, gesitueerd in\n"
"%s"

#: ../src/Package_Base.cpp:25
msgid "D-Mod filename (8 letters/numbers)"
msgstr "D-Modbestandsnaam (8 letters/cijfers)"

#. begin wxGlade: Package_Base::set_properties
#: ../src/Package_Base.cpp:39
msgid "DFArc - Package"
msgstr "DFArc – Pakket"

#: ../share/freedink-dfarc.desktop.in.h:1
msgid "DFArc - Dink frontend"
msgstr "DFArc – Dink frontend"

#: ../share/freedink-dfarc.desktop.in.h:2
msgid "Run, edit, install, remove and package D-Mods (Dink Modules)"
msgstr "D-Mods (Dink Modules) starten, bewerken, installeren, verwijderen en verpakken"

#: ../share/freedink-mime.xml.in.h:1
msgid "Packaged D-Mod"
msgstr "Verpakte D-Mod"

#~ msgid "ERROR"
#~ msgstr "FOUT"
